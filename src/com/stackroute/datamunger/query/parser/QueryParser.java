package com.stackroute.datamunger.query.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueryParser {

	private QueryParameter queryParameter = new QueryParameter();
	
	/*
	 * this method will parse the queryString and will return the object of
	 * QueryParameter class
	 */
	public QueryParameter parseQuery(String queryString) {
		
		QueryParser qp = new QueryParser();
		
		String fileName = qp.getFileName(queryString);
		queryParameter.setFile(fileName);
		System.out.println(queryParameter.getFile());
		
		//List<String> orderByFields = qp.getOrderByFields(queryString);
		queryParameter.setOrderByFields(qp.getOrderByFields(queryString));
		
		//List<String> groupByFields = qp.getGroupByFields(queryString);
		queryParameter.setGroupByFields(qp.getGroupByFields(queryString));

		//List<String> fieldsName = qp.getFields(queryString);
		queryParameter.setFields(qp.getFields(queryString));
		
		//List<Restriction> conditionParts = qp.getConditionParts(queryString);
		queryParameter.setRestictions(qp.getConditionParts(queryString));
		
		//List<String> logicalOperators = qp.getLogicalOperators(queryString);
		queryParameter.setLogicalOperators(qp.getLogicalOperators(queryString));
		
		//List<AggregateFunction> aggregateFunction = qp.getAggregateFunctions(queryString);
		queryParameter.setAggregateFunctions(qp.getAggregateFunctions(queryString));
		
		return queryParameter;
		
	}
	/*
	 * extract the name of the file from the query. File name can be found after the
	 * "from" clause.
	 */
	public String getFileName(String queryString) {
        String fileName = queryString.split(" from ")[1].split(" where ")[0].split(" group by ")[0].split(" order by ")[0].trim();
        return fileName;
    }
	
	/*
		 * extract the order by fields from the query string. Please note that we will
		 * need to extract the field(s) after "order by" clause in the query, if at all
		 * the order by clause exists. For eg: select city,winner,team1,team2 from
		 * data/ipl.csv order by city from the query mentioned above, we need to extract
		 * "city". Please note that we can have more than one order by fields.
		 */
		
	public List<String>  getOrderByFields(String queryString) {
		List<String>  orderByFields = new ArrayList<String>();
        if(queryString.contains(" order by "))
        {
        	orderByFields = Arrays.asList(queryString.split(" order by ")[1].split("\\s*,\\s*"));
        }
        return orderByFields;
    }
		
		/*
		 * extract the group by fields from the query string. Please note that we will
		 * need to extract the field(s) after "group by" clause in the query, if at all
		 * the group by clause exists. For eg: select city,max(win_by_runs) from
		 * data/ipl.csv group by city from the query mentioned above, we need to extract
		 * "city". Please note that we can have more than one group by fields.
		 */
		
	public List<String>  getGroupByFields(String queryString) {
		List<String>  groupByFields = new ArrayList<String>();
        if(queryString.contains(" group by "))
        {
        	groupByFields = Arrays.asList(queryString.split(" group by ")[1].split(" order by ")[0].split("\\s*,\\s*"));
        }

        return groupByFields;
    }
		
		/*
		 * extract the selected fields from the query string. Please note that we will
		 * need to extract the field(s) after "select" clause followed by a space from
		 * the query string. For eg: select city,win_by_runs from data/ipl.csv from the
		 * query mentioned above, we need to extract "city" and "win_by_runs". Please
		 * note that we might have a field containing name "from_date" or "from_hrs".
		 * Hence, consider this while parsing.
		 */
	public List<String> getFields(String queryString) {
		List<String>  fieldsName = new ArrayList<String>();
		fieldsName = Arrays.asList(queryString.split("select ")[1].split(" from ")[0].split("\\s*,\\s*"));
        return fieldsName;
    }
		
		/*
		 * extract the conditions from the query string(if exists). for each condition,
		 * we need to capture the following: 
		 * 1. Name of field 
		 * 2. condition 
		 * 3. value
		 * 
		 * For eg: select city,winner,team1,team2,player_of_match from data/ipl.csv
		 * where season >= 2008 or toss_decision != bat
		 * 
		 * here, for the first condition, "season>=2008" we need to capture: 
		 * 1. Name of field: season 
		 * 2. condition: >= 
		 * 3. value: 2008
		 * 
		 * the query might contain multiple conditions separated by OR/AND operators.
		 * Please consider this while parsing the conditions.
		 * 
		 */
	
	public List<Restriction> getConditionParts(String queryString) {
		List<Restriction> restrictions = new ArrayList<Restriction>();
		if(queryString.contains(" where ")) 
		{
			List<String> conditionParts = Arrays.asList(queryString.split(" where")[1].split(" group by ")[0].split(" order by ")[0].split(" and | or "));

			for(String sr : conditionParts)
			{
				String [] arr = sr.split("<=|>=|!=|<|>|=");
				String propertyName = arr[0].trim();
				String propertyValue = arr[1].replace("'", " ").trim();
				String condition = sr.split(propertyName)[1].replace("'", " ").split(propertyValue)[0].trim();
				Restriction res = new Restriction(propertyName,condition,propertyValue);
				restrictions.add(res);
			}
		}
		else
		{
			restrictions = null;
		}
        return restrictions;
    }
		
		/*
		 * extract the logical operators(AND/OR) from the query, if at all it is
		 * present. For eg: select city,winner,team1,team2,player_of_match from
		 * data/ipl.csv where season >= 2008 or toss_decision != bat and city =
		 * bangalore
		 * 
		 * the query mentioned above in the example should return a List of Strings
		 * containing [or,and]
		 */
		
	public List<String> getLogicalOperators(String queryString) {
		List<String>  logicalOperators = new ArrayList<String>();
		if(queryString.contains(" where "))
		{
			String sr = queryString.split(" where ")[1].split(" order by ")[0].split(" group by ")[0];
			if(sr.contains(" and ") && sr.contains(" or "))
			{
				logicalOperators.add("and");
				logicalOperators.add("or");
			}
			else if(sr.contains(" or "))
			{
				logicalOperators.add("or");
			}
			else if(sr.contains(" and "))
			{
				logicalOperators.add("and");
			}
			else
			{
				logicalOperators = null;
			}
		}
		else
		{
			logicalOperators = null;
		}
        return logicalOperators;
    }
		
		/*
		 * extract the aggregate functions from the query. The presence of the aggregate
		 * functions can determined if we have either "min" or "max" or "sum" or "count"
		 * or "avg" followed by opening braces"(" after "select" clause in the query
		 * string. in case it is present, then we will have to extract the same. For
		 * each aggregate functions, we need to know the following: 
		 * 1. type of aggregate function(min/max/count/sum/avg) 
		 * 2. field on which the aggregate function is being applied
		 * 
		 * Please note that more than one aggregate function can be present in a query
		 * 
		 * 
		 */
		
	public List<AggregateFunction> getAggregateFunctions(String queryString) {
		List<AggregateFunction>  aggregateFunction = new ArrayList<AggregateFunction>();
		
		if(queryString.contains("sum(") || queryString.contains("max(") || queryString.contains("min(") || queryString.contains("avg(") || queryString.contains("count("))
		{
			List<String> aggregrateFunction = Arrays.asList(queryString.split("select ")[1].split(" from ")[0].split("\\s*,\\s*"));
			for(String sr : aggregrateFunction)
			{
				if(sr.contains("sum(") || sr.contains("avg(") || sr.contains("count(") || sr.contains("max(") || sr.contains("min("))
				{
					String fieldName = sr.split("\\(")[1].split("\\)")[0];
					String FunctionName = sr.split("\\(")[0];
					AggregateFunction af = new AggregateFunction(fieldName,FunctionName);
					aggregateFunction.add(af);

				}
			}
		}
		else
		{
			AggregateFunction af = new AggregateFunction(null,null);
			aggregateFunction.add(af);

		}
        return aggregateFunction;
    }
	
	
}
