package com.stackroute.datamunger.query.parser;

import java.util.ArrayList;
import java.util.List;

/*
 * This class is used for storing name of field, condition and value for 
 * each conditions
 * */
public class Restriction {
	String  propertyName;
	String  condition;
	String  propertyValue;
	
	public Restriction(String propertyName,String  condition,String  propertyValue)
	{
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
		this.condition = condition;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	
	public String getPropertyValue() {
		return propertyValue;
	}

	public String getCondition() {
		return condition;
	}

}
