package com.stackroute.datamunger.query.parser;

/* This class is used for storing name of field, aggregate function for 
 * each aggregate function
 * */
public class AggregateFunction {

	String field;
	String function;
	
	public AggregateFunction(String field,String function) {
		this.field = field;
		this.function = function;
	}
	
	public String getField() {
		return field;
	}
	
	public String getFunction() {
		return function;
	}

}
